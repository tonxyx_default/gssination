/**
 * Created by toni on 26/05/16.
 */

/**
 * Select 2
 */
$(".select2-basic-multiple").select2();

/**
 * Message content population
 */
$(".populate-existing-message").on('change', function () {
  if ($(this).val() != '') {
    $.ajax({
      type: 'POST',
      url: '/message/fetchMessage',
      data: {id: $(this).val()},
      success: function (response) {
        $('#subject').val(response.subject);
        $('#message').val(response.message);
      },
      error: function (xhr, textStatus, errorThrown) {
        console.log(xhr);
        console.log(textStatus);
        console.log(errorThrown);
      }
    });
  } else {
    $('#subject').val('');
    $('#message').val('');
  }
});
