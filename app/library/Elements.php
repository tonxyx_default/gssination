<?php

use Phalcon\Mvc\User\Component;

/**
 * Elements
 *
 * Helps to build UI elements for the application
 */
class Elements extends Component {

  private $_headerMenu = [
    'navbar-left' => [
      'admin' => [
        'caption' => 'Admin',
        'action' => 'index',
      ],
      'message' => [
        'caption' => 'Messages',
        'action' => 'index',
      ],
      'user' => [
        'caption' => 'Users',
        'action' => 'index',
      ],
      'note' => [
        'caption' => 'Notes',
        'action' => 'index',
      ],
    ],
    'navbar-right' => [
      'session' => [
        'caption' => 'Log In/Sign Up',
        'action' => 'index',
      ],
    ],
  ];

  private $_tabs = [
    'user' => [
      'List users' => [
        'controller' => 'user',
        'action' => 'index',
        'any' => false,
        'class' => 'btn btn-primary'
      ],
      'List groups' => [
        'controller' => 'user',
        'action' => 'indexGroup',
        'any' => false,
        'class' => 'btn btn-primary'
      ],
      'Create user' => [
        'controller' => 'user',
        'action' => 'create',
        'any' => false,
        'class' => 'btn btn-success'
      ],
      'Create group' => [
        'controller' => 'user',
        'action' => 'createGroup',
        'any' => false,
        'class' => 'btn btn-success'
      ],
    ],
    'message' => [
      'List messages' => [
        'controller' => 'message',
        'action' => 'index',
        'any' => false,
        'class' => 'btn btn-primary'
      ],
      'Received messages' => [
        'controller' => 'message',
        'action' => 'indexReceived',
        'any' => false,
        'class' => 'btn btn-primary'
      ],
      'Create new message' => [
        'controller' => 'message',
        'action' => 'create',
        'any' => false,
        'class' => 'btn btn-success'
      ],
    ],
    'note' => [
      'List notes' => [
        'controller' => 'note',
        'action' => 'index',
        'any' => false,
        'class' => 'btn btn-primary'
      ],
      'Create note' => [
        'controller' => 'note',
        'action' => 'create',
        'any' => false,
        'class' => 'btn btn-success'
      ],
    ],
  ];

  /**
   * Builds header menu with left and right items
   *
   * @return string
   */
  public function getMenu () {

    $auth = $this->session->get('auth');
    if ($auth) {
      $this->_headerMenu['navbar-right']['session'] = [
        'caption' => 'Log Out',
        'action' => 'end',
      ];
    } else {
      unset($this->_headerMenu['navbar-left']);
    }

    $controllerName = $this->view->getControllerName();
    foreach ($this->_headerMenu as $position => $menu) {
      echo '<div class="nav-collapse">';
      echo '<ul class="nav navbar-nav ', $position, '">';
      foreach ($menu as $controller => $option) {
        if ($controllerName == $controller) {
          echo '<li class="active">';
        } else {
          echo '<li>';
        }
        echo $this->tag->linkTo($controller . '/' . $option['action'], $option['caption']);
        echo '</li>';
      }
      echo '</ul>';
      echo '</div>';
    }

  }

  /**
   * Returns menu tabs
   */
  public function getTabs () {
    $controllerName = $this->view->getControllerName();
    $actionName = $this->view->getActionName();
    echo '<ul class="nav nav-tabs">';
    foreach ($this->_tabs as $caption => $options) {
      if ($caption == $controllerName) {
        foreach ($options as $title => $option) {
          if ($option['action'] == $actionName || $option['any']) {
            echo '<li class="active">';
          } else {
            echo '<li>';
          }
          $class = isset($option['class']) ? $option['class'] : '';
          echo $this->tag->linkTo([$option['controller'] . '/' . $option['action'], $title, 'class' => $class]),
          '</li>';
        }
      }
    }
    echo '</ul>';
  }
}
