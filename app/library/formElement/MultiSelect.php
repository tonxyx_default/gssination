<?php

use Phalcon\Forms\Element\Select;

class MultiSelect extends Select {

  public function render ($attributes = null) {
    return parent::render(array_merge($attributes, [
      'name' => $this->getName() . '[]',
      'class' => 'select2-basic-multiple js-example-basic-multiple',
      'multiple' => 'multiple',
    ]));
  }
}
