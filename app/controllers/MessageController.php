<?php

class MessageController extends ControllerBase {

  public function initialize () {
    $this->tag->setTitle('Welcome to admin message interface');
    parent::initialize();
  }

  public function indexAction () {
    $this->view->messages = Message::find();
  }

  /**
   * Create new message
   *
   * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
   */
  public function createAction (){
    $this->view->form = new MessageForm;

    if ($this->request->isPost()) {
      $message = new Message();
      $message->status = Message::MESSAGE_SENT;

      if ($message->save($this->request->getPost()) == false) {
        foreach ($message->getMessages() as $mess) {
          $this->flash->error((string) $mess);
        }
      } else {
        $sid = "AC051b4dc4d7369ee339248b764ad3d189"; // Your Account SID from www.twilio.com/user/account
        $token = "2bc97e0e51723dcb1710d81ba6bb4132"; // Your Auth Token from www.twilio.com/user/account

        $client = new Services_Twilio($sid, $token);

        try {
          foreach ($this->request->getPost('to') as $to) {
            $client->account->messages->create(array(
              'To' => preg_replace('/^00/', '+', $to),
              'From' => "+12017201540",
              'Body' => $message->message,
            ));
          }
        } catch (Services_Twilio_RestException $e) {
          echo $e->getMessage();
        }

        return $this->response->redirect('message/index');
      }
    }
  }

  /**
   * Ajax action for fetching message content
   *
   * @return bool|\Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
   */
  public function fetchMessageAction (){
    $id = $this->request->getPost('id');
    $message = Message::findFirst(['id' => $id]);

    if ($this->request->isAjax()) {
      $this->response->setContentType('application/json', 'UTF-8');
      $this->response->setJsonContent([
        'status' => 'success',
        'subject' => $message->subject,
        'message' => $message->message,
      ]);
      return $this->response->send();
    }

    return false;
  }

  /**
   * View message
   *
   * @return bool|\Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
   */
  public function viewAction ($id){
    $this->view->message = $message = Message::findFirstById($id);
  }

  /**
   * List received messages
   */
  public function receiveMessageAction (){
    echo '<pre>'; var_dump($this->request->getPost()); die();
  }

  /**
   * List received messages
   */
  public function indexReceivedAction (){
    $this->view->receivedMessages = $receivedMessages = ReceivedMessage::find();
  }
}
