<?php

class AdminController extends ControllerBase {

  public function initialize () {
    $this->tag->setTitle('Welcome to admin interface');
    parent::initialize();
  }

  public function indexAction () {
    $this->view->users = User::find();
  }
}
