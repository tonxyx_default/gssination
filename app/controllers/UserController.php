<?php

class UserController extends ControllerBase {

  public function initialize () {
    $this->tag->setTitle('Welcome to user interface');
    parent::initialize();
  }

  public function indexAction () {
    $this->view->users = User::find();
  }

  public function createAction () {
    $form = new UserForm();

    if ($this->request->isPost()) {

      $name = $this->request->getPost('name', [
        'string',
        'striptags',
      ]);
      $username = $this->request->getPost('username', 'alphanum');
      $email = $this->request->getPost('email', 'email');
      $password = $this->request->getPost('password');
      $repeatPassword = $this->request->getPost('repeatPassword');
      $mobile = $this->request->getPost('mobile');
      $membership = $this->request->getPost('membership');
      $status = $this->request->getPost('status');
      $active = $this->request->getPost('active');

      if ($password != $repeatPassword) {
        $this->flash->error('Passwords are different');

        return false;
      }

      $user = new User();
      $user->username = $username;
      $user->password = sha1($password);
      $user->name = $name;
      $user->email = $email;
      $user->mobile = $mobile;
      $user->membership = $membership;
      $user->status = $status;
      $user->active = $active;
      $user->created_at = new Phalcon\Db\RawValue('now()');

      if ($user->save() == false) {
        foreach ($user->getMessages() as $message) {
          $this->flash->error((string) $message);
        }
      }

      return $this->response->redirect('user/index');
    }

    $this->view->form = $form;
  }

  public function editAction ($id){
    $user = User::findFirstById($id);

    $form = $this->view->form = new UserForm($user);

    $form->getEntity()->password = '';
    $form->getEntity()->repeatPassword = '';

    if ($this->request->isPost()) {
      $post = $this->request->getPost();
      if ($form->isValid($post)) {

        $password = $post['password'];
        $repeatPassword = $post['repeatPassword'];

        if ($password != '' && $repeatPassword != '' && $password != $repeatPassword) {
          $this->flash->error('Passwords are different');

          return false;
        } else {
          echo '<pre>'; var_dump($user->password); die();
          $post['password'] = $user->password;
          $post['repeatPassword'] = $user->repeatPassword;
        }

        if ($user->update($post) == false) {
          foreach ($user->getMessages() as $mess) {
            $this->flash->error((string) $mess);
          }
        }

        return $this->response->redirect('user/index');
      } else {
        foreach ($form->getMessages() as $message) {
          $this->flash->error((string) $message);
        }
      }
    }

    $this->view->pick('user/create');
  }

  public function indexGroupAction () {
    $this->view->groups = Group::find();
  }

  public function createGroupAction (){
    $form = $this->view->form = new GroupForm();

    if ($this->request->isPost()) {
      $post = $this->request->getPost();
      if ($form->isValid($post)) {
        $group = new Group();

        if ($group->save($post) == false) {
          foreach ($group->getMessages() as $mess) {
            $this->flash->error((string) $mess);
          }
        }

        UserGroup::ensure($group, $post['user']);

        return $this->response->redirect('user/indexGroup');
      } else {
        foreach ($form->getMessages() as $message) {
          $this->flash->error((string) $message);
        }
      }
    }
  }

  public function editGroupAction ($id){
    $form = $this->view->form = new GroupForm();

    $group = Group::findFirstById($id);
    $form->setEntity($group);
    $form->get('user')->setDefault(UserGroup::getGroupUsers($id));

    if ($this->request->isPost()) {
      $post = $this->request->getPost();
      if ($form->isValid($post)) {

        if ($group->save($post) == false) {
          foreach ($group->getMessages() as $mess) {
            $this->flash->error((string) $mess);
          }
        }

        UserGroup::ensure($group, $post['user']);

        return $this->response->redirect('user/indexGroup');
      } else {
        foreach ($form->getMessages() as $message) {
          $this->flash->error((string) $message);
        }
      }
    }

    $this->view->pick('user/createGroup');
  }


}
