<?php

class NoteController extends ControllerBase {

  public function indexAction () {
    $this->view->notes = Note::find();
  }

  public function createAction (){
    $form = $this->view->form = new NoteForm();

    if ($this->request->isPost()) {
      $post = $this->request->getPost();
      if ($form->isValid($post)) {
        $note = new Note();

        if ($note->save($post) == false) {
          foreach ($note->getMessages() as $mess) {
            $this->flash->error((string) $mess);
          }
        }

      return $this->response->redirect('note/index');
      } else {
        foreach ($form->getMessages() as $message) {
          $this->flash->error((string) $message);
        }
      }
    }
  }

  public function editAction ($id){
    $form = $this->view->form = new NoteForm();

    $note = Note::findFirstById($id);
    $form->setEntity($note);

    if ($this->request->isPost()) {
      $post = $this->request->getPost();
      if ($form->isValid($post)) {

        if ($note->update($post) == false) {
          foreach ($note->getMessages() as $mess) {
            $this->flash->error((string) $mess);
          }
        }

        return $this->response->redirect('note/index');
      } else {
        foreach ($form->getMessages() as $message) {
          $this->flash->error((string) $message);
        }
      }
    }

    $this->view->pick("note/create");
  }

}
