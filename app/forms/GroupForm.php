<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;

class GroupForm extends Form {

  public function initialize ($entity = null, $options = null) {
    // Name
    $name = new Text('name');
    $name->setLabel('Group name');
    $name->setFilters([
      'striptags',
      'string',
    ]);
    $name->addValidators([
      new PresenceOf([
        'message' => 'Name is required',
      ]),
    ]);
    $this->add($name);

    $user = new MultiSelect('user');
    $user->setLabel('Users');
    $user->setOptions(User::getSelectOptionsIdName());
    $user->addValidators([
      new PresenceOf([
        'message' => 'Please choose users',
      ]),
    ]);
    $this->add($user);

    // Active
    $active = new Select('active');
    $active->setLabel('Active');
    $active->setOptions(Group::$activeStatuses);
    $active->setDefault(1);
    $this->add($active);
  }
}
