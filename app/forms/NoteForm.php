<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;

class NoteForm extends Form {

  public function initialize ($entity = null, $options = null) {
    // Title
    $title = new Text('title');
    $title->setLabel('Title');
    $title->setFilters([
      'striptags',
      'string',
    ]);
    $title->addValidators([
      new PresenceOf([
        'message' => 'Title is required',
      ]),
    ]);
    $this->add($title);

    // Note
    $note = new TextArea('note');
    $note->setLabel('Note');
    $note->addValidators([
      new PresenceOf([
        'message' => 'Please enter your note',
      ]),
    ]);
    $this->add($note);
  }
}
