<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;

class MessageForm extends Form {

  public function initialize ($entity = null, $options = null) {

    // Insert existing
    $existing = new Select('existing');
    $existing->setLabel('Choose existing');
    $existing->setOptions(array_merge(['' => '-'], Message::getSelectOptions()));
    $this->add($existing);

    // Subject
    $subject = new Text('subject');
    $subject->setLabel('Subject');
    $subject->setFilters([
      'striptags',
      'string',
    ]);
    $subject->addValidators([
      new PresenceOf([
        'message' => 'Subject is required',
      ]),
    ]);
    $this->add($subject);

    // Message
    $message = new TextArea('message');
    $message->setLabel('Message');
    $message->addValidators([
      new PresenceOf([
        'message' => 'Please enter your message',
      ]),
    ]);
    $this->add($message);

    // To
    $to = new MultiSelect('to');
    $to->setLabel('To');
    $to->setOptions(User::getSelectOptionsMobileName());
    $this->add($to);

    // Group
    $group = new MultiSelect('group');
    $group->setLabel('Group');
    $group->setOptions(Group::getSelectOptionsIdName());
    $this->add($group);
  }
}
