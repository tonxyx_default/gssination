<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;

class UserForm extends Form {

  public function initialize ($entity = null, $options = null) {
    // Name
    $name = new Text('name');
    $name->setLabel('Your Full Name');
    $name->setFilters([
      'striptags',
      'string',
    ]);
    $name->addValidators([
      new PresenceOf([
        'message' => 'Name is required',
      ]),
    ]);
    $this->add($name);

    // Name
    $username = new Text('username');
    $username->setLabel('Username');
    $username->setFilters([
      'striptags',
      'string',
    ]);
    $username->addValidators([
      new PresenceOf([
        'message' => 'Please enter your desired user name',
      ]),
    ]);
    $this->add($username);

    // Email
    $email = new Text('email');
    $email->setLabel('E-Mail');
    $email->setFilters('email');
    $email->addValidators([
      new PresenceOf([
        'message' => 'E-mail is required',
      ]),
      new Email([
        'message' => 'E-mail is not valid',
      ]),
    ]);
    $this->add($email);

    // Password
    $password = new Password('password');
    $password->setLabel('Password');
    if ($entity == null)
      $password->addValidators([
        new PresenceOf([
          'message' => 'Password is required',
        ]),
      ]);
    $this->add($password);

    // Confirm Password
    $repeatPassword = new Password('repeatPassword');
    $repeatPassword->setLabel('Repeat Password');
    if ($entity == null)
      $repeatPassword->addValidators([
        new PresenceOf([
          'message' => 'Confirmation password is required',
        ]),
      ]);
    $this->add($repeatPassword);

    // Mobile
    $mobile = new Text('mobile');
    $mobile->setLabel('Mobile phone');
    $mobile->addValidators([
      new PresenceOf([
        'message' => 'Mobile is required',
      ]),
    ]);
    $this->add($mobile);

    // Membership
    $membership = new Select('membership');
    $membership->setLabel('Membership');
    $membership->setOptions(User::$userMemberships);
    $this->add($membership);

    // Status
    $status = new Select('status');
    $status->setLabel('Status');
    $status->setOptions(User::$userStatus);
    $this->add($status);

    // Active
    $active = new Select('active');
    $active->setLabel('Active');
    $active->setOptions(User::$activeStatuses);
    $this->add($active);
  }
}
