<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class User extends Model {

  /**
   * User active statuses
   *
   * @var array
   */
  static $activeStatuses = [
    0 => 'Inactive',
    1 => 'Active',
  ];

  /**
   * User status (qualification)
   *
   * @var array
   */
  static $userStatus = [
    0 => 'Suradnik',
    1 => 'Pripravnik',
    2 => 'Spašavatelj',
  ];

  /**
   * User membership
   *
   * @var array
   */
  static $userMemberships = [
    'A' => 'A',
    'B' => 'B',
  ];

  /**
   * Validate user input
   *
   * @return bool
   */
  public function validation () {
    $this->validate(new EmailValidator([
      'field' => 'email',
    ]));

    $this->validate(new UniquenessValidator([
      'field' => 'email',
      'message' => 'Sorry, The email was registered by another user',
    ]));

    $this->validate(new UniquenessValidator([
      'field' => 'username',
      'message' => 'Sorry, That username is already taken',
    ]));

    if ($this->validationHasFailed() == true) {
      return false;
    }
  }

  /**
   * Get select options mobile - name
   *
   * @return mixed
   */
  static public function getSelectOptionsMobileName () {
    $user = new self();
    return $user->prepareForSelectMobileName();
  }

  /**
   * Prepare messages for select
   *
   * @return mixed
   */
  public function prepareForSelectMobileName () {
    return $this->getDI()->get('db')->query('SELECT mobile, name FROM user;')->fetchAll(PDO::FETCH_KEY_PAIR);
  }

  /**
   * Get select options id - name
   *
   * @return mixed
   */
  static public function getSelectOptionsIdName () {
    $user = new self();
    return $user->prepareForSelectIdName();
  }

  /**
   * Prepare messages for select
   *
   * @return mixed
   */
  public function prepareForSelectIdName () {
    return $this->getDI()->get('db')->query('SELECT id, name FROM user;')->fetchAll(PDO::FETCH_KEY_PAIR);
  }
}
