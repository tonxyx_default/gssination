<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class UserGroup extends Model {

  public $group;

  /**
   * Get select options id - name
   *
   * @return mixed
   */
  static public function getSelectOptionsIdName () {
    $group = new self();
    return $group->prepareForSelectIdName();
  }

  /**
   * Prepare messages for select
   *
   * @return mixed
   */
  public function prepareForSelectIdName () {
//    return $this->getDI()->get('db')->query('SELECT id, name FROM group;')->fetchAll(PDO::FETCH_KEY_PAIR);
  }

  /**
   * Ensure group user entity
   *
   * @param $group
   * @param $users
   *
   * @throws Exception
   */
  static public function ensure ($group, $users) {

    try {
      self::find(['group' => $group->id])->delete();

      foreach ($users as $user) {
        $userGroup = new self();
        $userGroup->group = $group->id;
        $userGroup->user = $user;
        $userGroup->save();
      }
    } catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * Get all users from group
   */
  static public function getGroupUsers ($group){
    $users = [];
    foreach (self::find(['group' => $group]) as $g) {
      $users[] = $g->user;
    }

    return $users;
  }

}
