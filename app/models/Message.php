<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class Message extends Model {

  const MESSAGE_SENT = 1;

  /**
   * Get select options
   */
  static public function getSelectOptions () {
    $message = new self();
    return $message->prepareForSelect();
  }

  /**
   * Prepare messages for select
   */
  public function prepareForSelect () {
    return $this->getDI()->get('db')->query('SELECT id, subject FROM message;')->fetchAll(PDO::FETCH_KEY_PAIR);
  }
}
