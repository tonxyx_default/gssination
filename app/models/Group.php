<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class Group extends Model {

  /**
   * Group active statuses
   *
   * @var array
   */
  static $activeStatuses = [
    0 => 'Inactive',
    1 => 'Active',
  ];

  /**
   * Get select options id - name
   *
   * @return mixed
   */
  static public function getSelectOptionsIdName () {
    $group = new self();
    return $group->prepareForSelectIdName();
  }

  /**
   * Prepare messages for select
   *
   * @return mixed
   */
  public function prepareForSelectIdName () {
    return $this->getDI()->get('db')->query('SELECT id, name FROM `group` where active = :active;', ['active' => 1])
      ->fetchAll(PDO::FETCH_KEY_PAIR);
  }
}
