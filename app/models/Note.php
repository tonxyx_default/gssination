<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class Note extends Model {

  public function initialize () {
    $this->user = $this->getDI()->get('session')->get('auth')['id'];
  }

  /**
   * @inheritdoc
   */
  public function update ($data = null, $whiteList = null){
    // todo - quickfix for saving edit time
    $this->edited_at = date('Y-m-d h:i:s');
    parent::update();
  }
}
