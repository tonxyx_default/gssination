<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class ReceivedMessage extends Model {

  /**
   * Get select options
   */
  static public function getSelectOptions () {
    $message = new self();
    return $message->prepareForSelect();
  }

  /**
   * Prepare messages for select
   */
  public function prepareForSelect () {
    return $this->getDI()->get('db')->query('SELECT id, subject FROM received_message;')->fetchAll(PDO::FETCH_KEY_PAIR);
  }
}
